package com.service;

import com.model.Employee;

public interface EmployeeService {

	Employee addEmployee(Employee employee);
	
	public abstract Employee displayEmployeedata(Employee employee);
	
	Employee[] getAllEmployees();
	
	Employee[] searchEmployeeByName(String empName);
	
	Employee searchById(int empId);
	
	Employee[] searchEmployeeByRole(String role);
	
	Employee[] searchEmployeeByExperience(int experience);
	
}
