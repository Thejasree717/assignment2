package com.service;

import com.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee addEmployee(Employee employee) {
		
		return null;
	}

	@Override
	public Employee displayEmployeedata(Employee employee) {
		System.out.println("Employee Data");
		System.out.println("Emp Id : "+employee.getEmpId());
		System.out.println("Emp Name : "+employee.getEmpName());
		System.out.println("Role : "+employee.getRole());
		System.out.println("Experience : "+employee.getExperience());
		return employee;
		
	}

	@Override
	public Employee[] getAllEmployees() {
		
		Employee employee1 = new Employee(10, "Software Engineer", 2, "Theja");
		Employee employee2 = new Employee(20, "Software Developer", 3, "Rama");
		Employee employee3 = new Employee(30, "Testing", 2, "Sita");
		Employee employee4 = new Employee(40, "HR", 3, "Lakshamana");
		Employee employee5 = new Employee(50, "Testing", 2, "ABC");
		
		Employee[] employees = new Employee[5];
		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;
		employees[3] = employee4;
		employees[4] = employee5;
		return employees;
	}

	@Override
	public Employee[] searchEmployeeByName(String empName) {
		Employee[] employees = this.getAllEmployees();
		Employee[] employeesByName = new Employee[3];
		int k=0;
		for(int i=0; i< employees.length; i++) {
			if(employees[i].getEmpName().equalsIgnoreCase(empName)) {
				if(k<employeesByName.length) {
					employeesByName[k] = employees[i];
					k++;
				}
				else {
					break;
				}
			}
		}
		return employeesByName;
	}

	@Override
	public Employee searchById(int empId) {
		Employee[] employees = this.getAllEmployees();
		Employee employeeById = new Employee();
		for(int i=0; i<employees.length; i++) {
			if(employees[i].getEmpId() == empId) {
				employeeById = employees[i];
			}
		}
		return employeeById;
	}

	@Override
	public Employee[] searchEmployeeByRole(String role) {
		Employee[] employees = this.getAllEmployees();
		Employee[] employeesByRole = new Employee[2];
		int k=0;
		for(int i=0; i<employees.length; i++) {
			if(employees[i].getRole().equalsIgnoreCase(role)) {
				if(k<employeesByRole.length) {
					employeesByRole[k] = employees[i];
					k++;
				}
				else {
					break;
				}
			}
		}
		return employeesByRole;
	}

	@Override
	public Employee[] searchEmployeeByExperience(int experience) {
		Employee[] employees = this.getAllEmployees();
		Employee[] employeesByExperience = new Employee[2];
		int k=0;
		for(int i=0; i<employees.length; i++) {
			if(employees[i].getExperience() == experience) {
				if(k<employeesByExperience.length) {
					employeesByExperience[k] = employees[i];
					k++;
				}
				else {
					break;
				}
			}
		}
		return employeesByExperience;
	}

}
