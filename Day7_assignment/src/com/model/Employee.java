package com.model;

public class Employee {

	private int empId;
	
	private String role;
	
	private int experience;
	
	private String empName;


	public Employee(int empId, String role, int experience, String empName) {
		super();
		this.empId = empId;
		this.role = role;
		this.empName = empName;
		this.experience = experience;
	}

	public Employee() {
		super();
	}
	
	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}
	
	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}
}
