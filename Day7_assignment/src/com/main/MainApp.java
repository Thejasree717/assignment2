package com.main;

import java.util.Scanner;

import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class MainApp {

	public static void main(String[] args) {
		
		EmployeeService employeeService = new EmployeeServiceImpl();
		
		//Searching Employees by role
		System.out.println("*********Search Employee By Role**********");
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter role: ");
		String role = sc.nextLine();
		Employee[] employeesByRole = employeeService.searchEmployeeByRole(role);
		for(int i=0; i<employeesByRole.length;i++) {
			if(employeesByRole[i]!= null) {
				System.out.print((i+1)+".");
				employeeService.displayEmployeedata(employeesByRole[i]);
			}
			else {
				break;
			}
		}
		
		
		//Searching Employees by Name
				System.out.println("*********Search Employee By name**********");
				Scanner sc1 = new Scanner(System.in);
				System.out.println("Enter Name: ");
				String empName = sc.nextLine();
				Employee[] employeesByName = employeeService.searchEmployeeByName(empName);
				for(int i=0; i<employeesByName.length;i++) {
					if(employeesByName[i]!= null) {
						System.out.print((i+1)+".");
						employeeService.displayEmployeedata(employeesByName[i]);
					}
					else {
						break;
					}
				}
				
				
				//Searching Employees by Experience
				System.out.println("*********Search Employee By Experience**********");
				Scanner sc2 = new Scanner(System.in);
				System.out.println("Enter Experience: ");
				int empExperience = sc.nextInt();
				Employee[] employeesByExperience = employeeService.searchEmployeeByExperience(empExperience);
				if(employeesByExperience != null) {
				for(int i=0; i<employeesByExperience.length;i++) {
					if(employeesByExperience[i] != null) {
						System.out.print((i+1)+".");
						employeeService.displayEmployeedata(employeesByExperience[i]);
					}
					else {
						break;
					}
				}
				}

	}

}
