package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.model.User;

@RestController
public class AnotherController {

	private static final Logger LOGGER = LogManager.getLogger(AnotherController.class);

	@GetMapping(value= "searchById/{alias}/{name}")
	public User getUserById(@PathVariable("alias") int userId, @PathVariable String name) {
		System.out.println("name you entered : "+name);
		User user =null;
		if(userId == 10) {
			LOGGER.info("inside getUserById() "+userId);
			user = new User(10, "Hello", "World");
		}
		if(userId == 20) {
			user = new User(20, "Good", "Morning");
		}
		return user;
	}
	
	//accept user detail(userId, userName, password)
	
	//@RequestMapping(value="create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PostMapping(value="create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> createUser(@RequestBody User user) {
		
		System.out.println("Request body : "+user.getUserId());
		System.out.println("Request body : "+user.getUserName());
		System.out.println("Request body : "+user.getPassword());
		user.setUserName("Welcome "+user.getUserName());
		ResponseEntity<User> responseEntity = new ResponseEntity<>(user, HttpStatus.CREATED);
		return responseEntity;
	}
	
	@GetMapping(value="/all")
	public List<User> getAllUser() {
		User user1 = new User(10, "Hello", "world");
		User user2 = new User(25, "Twenty", "five");
		User user3 = new User(34, "Thirty", "four");
		User user4 = new User(42, "Fourty", "two");
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		users.add(user3);
		users.add(user4);
		return users;
	}
}
