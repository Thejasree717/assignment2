package com.main;

import com.model.Employee;
import com.service.EmployeeService;

public class EmployeeApp {

	public static void main(String[] args) {
		
		Employee[] employees = EmployeeService.getAllEmployee();
		System.out.println("Fetching all employee data");
		for (int i = 0; i < employees.length; i++) {
			System.out.println(employees[i].getEmpNo());
			System.out.println(employees[i].getEmpName());
			System.out.println(employees[i].getSalary());
		}
		
		System.out.println("Constructor demo");
		Employee employee1 = new Employee();
		System.out.println(employee1.getEmpNo());
		System.out.println(employee1.getEmpName());
		System.out.println(employee1.getSalary());

		System.out.println("Constructor with arguments");
		Employee employee2 = new Employee(4, "Four", 4000);
		System.out.println(employee2.getEmpNo());
		System.out.println(employee2.getEmpName());
		System.out.println(employee2.getSalary());
		
		System.out.println("copy constructor");
		
		Employee employee3 = new Employee(employee2);
		System.out.println(employee3.getEmpNo());
		System.out.println(employee3.getEmpName());
		System.out.println(employee3.getSalary());
	}

}
