package com.service;

import com.model.Employee;

public class EmployeeService {

	public static Employee[] getAllEmployee() {
		
		Employee employee1 = new Employee();
		employee1.setEmpNo(1);
		employee1.setEmpName("One");
		employee1.setSalary(1000f);
		
		Employee employee2 = new Employee();
		employee2.setEmpNo(2);
		employee2.setEmpName("Two");
		employee2.setSalary(2000f);
		
		Employee employee3 = new Employee();
		employee3.setEmpNo(3);
		employee3.setEmpName("Three");
		employee3.setSalary(3000f);
		
		Employee[]  employees = new Employee[3];
		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;
		
		
		return employees;
	}
}
