package com.model;

public class Employee {

	private int empNo;
	private String empName;
	private float salary;
	
	public Employee() {
		this.empNo = 123;
		this.empName = "Constructor";
		this.salary = 1500;
	}
	
	public Employee(int empNo) {
		super();
		this.empNo = empNo;
	}

	
	public Employee(int empNo, String empName) {
		super();
		this.empNo = empNo;
		this.empName = empName;
	}

	
	public Employee(int empNo, String empName, float salary) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
	}

	public Employee(Employee emp) {
		this.empNo = emp.empNo;
		this.empName = emp.empName;
		this.salary = emp.salary;
	}
	
	public int getEmpNo() {
		return empNo;
	}
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	
}
