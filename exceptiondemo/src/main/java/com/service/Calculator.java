package com.service;

import java.io.IOException;

import com.exception.AppException;

public interface Calculator {

	public abstract int div(int num1, int num2) throws AppException, IOException;

}
