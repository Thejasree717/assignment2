package com.service;

import java.io.IOException;
import java.lang.*;

import com.exception.AppException;

public class CalculatorImpl implements Calculator {

	@Override
	public int div(int num1, int num2) throws AppException, IOException {

		int result = 0;
		if (num1 > 0 && num2 > -1) {

			try {
				result = num1 / num2;
			} catch (java.lang.ArithmeticException ae) {
				System.err.println("Solution " +ae.getMessage());
			}
		} else {

			throw new AppException("Please enter valid number");
		}
		return result;
	}

}
