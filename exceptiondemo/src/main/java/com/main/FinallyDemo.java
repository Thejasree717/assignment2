package com.main;

public class FinallyDemo {

	public static void main(String[] args) {
		Employee employee = null;
		int result=0;
		try {
		 employee = new Employee();
		 
		 int a = Integer.parseInt(args[0]);
		 int b = Integer.parseInt(args[1]);
		 
		result = a/b;
		System.out.println(result);
		}catch(ArithmeticException | ArrayIndexOutOfBoundsException | NumberFormatException one) {
			System.err.println(one.getMessage());
		}
		
		catch(Exception e) {
			System.err.println(e.getMessage());
		}
		finally {
			
			employee = null;
			System.out.println("Whether exception is there or not finally would be executed");
		}
	}

}
