package com.main;

import java.io.IOException;

import com.exception.AppException;
import com.service.Calculator;
import com.service.CalculatorImpl;

public class CalculatorMain {

	public static void main(String[] args)  {
		
		Calculator calculator = new CalculatorImpl();
		int resultOfDivision = 0;
		try {
			resultOfDivision = calculator.div(10,-5);
		} catch (AppException ae) {
			System.err.println(ae.getMessage());
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Division of two numbers : "+resultOfDivision);
		
		//int resultOfDivision1 = calculator.div(10,0);
		//System.out.println("Division of two numbers : "+resultOfDivision1);
		
		System.out.println("The End");
	}

}
