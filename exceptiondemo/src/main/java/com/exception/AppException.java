package com.exception;

public class AppException extends Exception {

	private String message;
	
	
	public AppException(String message) {
		super();
		this.message = message;
	}


	@Override
	public String getMessage() {
		
		return this.message;
	}

}
