package com.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public List searchEmployeeBySalary(List listOfEmployee, float salary) {
		
		List employees = new ArrayList();
		for (Iterator iterator = listOfEmployee.iterator(); iterator.hasNext();) {
			Employee object = (Employee) iterator.next();
			if(object.getSalary() > salary) {
				
				employees.add(object);
			}
			
		}
		return employees;
	}
}
