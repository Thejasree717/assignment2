package com.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class EmployeeMain {

	public static void main(String[] args) {
		Employee employee1 = new Employee(10, "Ten", 1000f);
		Employee employee2 = new Employee(20, "Twenty", 2000f);
		Employee employee3 = new Employee(30, "Thirty", 3000f);
		Employee employee4 = new Employee(40, "Fourty", 4000f);

		List list = new ArrayList();
		list.add(employee1);
		list.add(employee2);
		list.add(employee3);
		list.add(employee4);
		
		EmployeeService employeeService = new EmployeeServiceImpl();
		
		List ans = employeeService.searchEmployeeBySalary(list, 2000);
		for (Iterator iterator = ans.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			if (object instanceof Employee) {
				Employee employees = (Employee) object;
				System.out.println("Employee number : " +employees.getEmpNo());
				System.out.println("Employee name : " +employees.getEmpName());
				System.out.println("Employee Salary : " +employees.getSalary());
			}
			
		}
	}

}
