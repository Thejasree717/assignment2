package com.main;

import java.util.Scanner;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.exception.UserException;
import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

public class LoginApp {

	public static void main(String[] args) throws UserException {

		Scanner input = null;
		UserService userService = new UserServiceImpl();

		// creating user
		input = new Scanner(System.in);
		System.out.println("**********Create user************");
		System.out.println("Enter userid: ");
		int userId = input.nextInt();
		System.out.println("Enter username :");
		String userName = input.next();
		System.out.println("Enter Password: ");
		String password = input.next();
		User user = new User(userId, userName, password);
		try {
			User user1 = userService.createUser(user);
			if (user1 != null) {
				System.out.println("User is created" + user1.toString());
			}
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}

		// Read user by id
		User user2 = null;
		System.out.println("************Read user by Id**************");
		System.out.println("Enter userid: ");
		int userId1 = input.nextInt();
		try {
			user2 = userService.readUserById(userId1);
			if (user2.getUserId() != 0) {
				System.out.println("User found");
				System.out.println("User id : " + user2.getUserId());
				System.out.println("User name :" + user2.getUserName());
			}
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}

		// Read user by name
		System.out.println("*****************Read user by name************");
		System.out.println("Enter userName: ");
		String userName1 = input.next();
		try {
			User user3 = userService.readUserByName(userName1);
			if (user3.getUserName() != null) {
				System.out.println("User found");
				System.out.println("User id : " + user3.getUserId());
				System.out.println("User name :" + user3.getUserName());
			}
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}

		// Authenticating user by userid and password
		try {
			input = new Scanner(System.in);
			System.out.println("***********Authenticating user by userid and password************");
			System.out.println("Enter userid: ");
			int userId2 = input.nextInt();
			System.out.println("Enter Password: ");
			String password2 = input.next();
			userService = new UserServiceImpl();
			try {
				User user1 = userService.authenticateUserIdAndPassword(userId2, password2);
				// System.out.println("User id : "+user.getUserId());
				if (user1 != null) {
					System.out.println("Welcome : " + user1.getUserName());
				}
			} catch (UserException e) {
				System.err.println(e.getMessage());

			}
		} finally {
			input = null;
			userService = null;
		}

		// Update user
		Scanner input2 = new Scanner(System.in);
		System.out.println("********Update user using user_id***************");
		System.out.println("Enter userid: ");
		int userId2 = input2.nextInt();
		System.out.println("Enter new username :");
		String userName2 = input2.next();
		System.out.println("Enter new Password: ");
		String password2 = input2.next();
		User user3 = new User(userId2, userName2, password2);
		try {
			UserService userService2 = new UserServiceImpl();
			User user4 = userService2.updateUser(user3);
			if (user4 != null) {
				System.out.println("User is updated");
			}
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}

		// Delete the user
		System.out.println("***********Delete the user using user_id**************");
		System.out.println("Enter userid: ");
		int userId3 = input2.nextInt();
		try {
			UserService userService3 = new UserServiceImpl();
			int user5 = userService3.deleteById(userId3);
			if (user5 != 0) {
				System.out.println("User deleted " + userId3);
			}
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}

	}
}
