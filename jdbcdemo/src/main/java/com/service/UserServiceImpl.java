package com.service;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.exception.UserException;
import com.model.User;

public class UserServiceImpl implements UserService {

	@Override
	public User createUser(User user) throws UserException {
		UserDao userDao = new UserDaoImpl();
		user = userDao.createUserDao(user);
		if (user.getUserId() != 0 && user.getUserName() != null && user.getPassword() != null) {
			return user;
		} else {
			throw new UserException("No user is created");
		}
	}

	@Override
	public User readUserById(int userId) throws UserException {
		User user = null;
		UserDao userDao = new UserDaoImpl();
		user = userDao.readUserByIdDao(userId);
		if (user.getUserId() != 0) {
			return user;
		} else {
			throw new UserException("No user with the id: " + userId);
		}
	}

	@Override
	public User readUserByName(String userName) throws UserException {
		User user = null;
		UserDao userDao = new UserDaoImpl();
		user = userDao.readUserByNameDao(userName);
		if (user.getUserName() != null) {
			return user;
		} else {
			throw new UserException("No user with the name: " + userName);
		}
	}

	@Override
	public User authenticateUserIdAndPassword(int userId, String password) throws UserException {
		User user = null;
		int lengthOfUserId = String.valueOf(userId).length();
		int lengthOfPassword = password.length();
		if (lengthOfUserId >= 5 && lengthOfPassword >= 5) {
			UserDao userDao = new UserDaoImpl();
			user = userDao.authenticateUserIdAndPasswordDao(userId, password);
		} else {
			throw new UserException("Invalid userid or password");
		}
		return user;

	}

	@Override
	public User updateUser(User user) throws UserException {

		UserDao userDao = new UserDaoImpl();
		user = userDao.updateUserDao(user);
		if (user.getUserId() != 0) {
			return user;
		} else {
			throw new UserException("User not updated");
		}
	}

	@Override
	public int deleteById(int userId) throws UserException {
		UserDao userDao = new UserDaoImpl();
		int user = userDao.deleteByIdDao(userId);
		if (user != 0) {
			return user;
		} else {
			throw new UserException("User not deleted");
		}
	}

}
