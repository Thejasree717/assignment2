package com.service;

import com.exception.UserException;
import com.model.User;

public interface UserService {

	public abstract User createUser(User user) throws UserException;

	public abstract User readUserById(int userId) throws UserException;

	public abstract User readUserByName(String userName) throws UserException;

	public abstract User authenticateUserIdAndPassword(int userId, String password) throws UserException;

	public abstract User updateUser(User user) throws UserException;

	public abstract int deleteById(int userId) throws UserException;
}
