package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.exception.UserException;
import com.main.LoginApp;
import com.model.User;

public class UserDaoImpl implements UserDao {

	@Override
	public User createUserDao(User user) throws UserException {

		Connection connection = MySQLConnectionCheck.getConnection();
		String readQuery = "INSERT INTO user (user_id, user_name, password)" + " VALUES (?, ?, ?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(readQuery);
			preparedStatement.setInt(1, user.getUserId());
			preparedStatement.setString(2, user.getUserName());
			preparedStatement.setString(3, user.getPassword());
			Boolean resultSet = preparedStatement.execute();
			user = new User(user.getUserId(), user.getUserName(), user.getPassword());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User readUserByIdDao(int userId) throws UserException {
		User user = null;
		Connection connection = MySQLConnectionCheck.getConnection();
		String readQuery = "SELECT *FROM user where user_id = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(readQuery);
			preparedStatement.setInt(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();
			user = new User();
			while (resultSet.next()) {
				user.setUserId(resultSet.getInt("user_id"));
				user.setUserName(resultSet.getString("user_name"));
				user.setPassword(resultSet.getString("password"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User readUserByNameDao(String userName) throws UserException {
		User user = null;
		Connection connection = MySQLConnectionCheck.getConnection();
		String readQuery = "SELECT *FROM user where user_name = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(readQuery);
			preparedStatement.setString(1, userName);
			ResultSet resultSet = preparedStatement.executeQuery();
			user = new User();
			while (resultSet.next()) {
				user.setUserId(resultSet.getInt("user_id"));
				user.setUserName(resultSet.getString("user_name"));
				user.setPassword(resultSet.getString("password"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User authenticateUserIdAndPasswordDao(int userId, String password) throws UserException {
		User user = null;
		Connection connection = MySQLConnectionCheck.getConnection();
		String readQuery = "select *from user where user_id = ? and password = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(readQuery);
			preparedStatement.setInt(1, userId);
			preparedStatement.setString(2, password);
			ResultSet resultSet = preparedStatement.executeQuery();
			user = new User();
			while (resultSet.next()) {
				// System.out.println(resultSet.getInt("user_id"));
				user.setUserId(resultSet.getInt("user_id"));
				// System.out.println(resultSet.getString("user_name"));
				user.setUserName(resultSet.getString("user_name"));
				// System.out.println(resultSet.getString("password"));
				user.setPassword(resultSet.getString("password"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User updateUserDao(User user) throws UserException {
		Connection connection = MySQLConnectionCheck.getConnection();
		String readQuery = "UPDATE user set user_name = ?, password = ? where user_id = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(readQuery);
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setInt(3, user.getUserId());
			int resultSet = preparedStatement.executeUpdate();
			user = new User();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public int deleteByIdDao(int userId) throws UserException {
		User user = null;
		int result = 0;
		Connection connection = MySQLConnectionCheck.getConnection();
		String readQuery = "DELETE FROM user where user_id = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(readQuery);
			preparedStatement.setInt(1, userId);
			result = preparedStatement.executeUpdate();
			user = new User();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
