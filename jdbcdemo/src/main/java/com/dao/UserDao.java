package com.dao;

import com.exception.UserException;
import com.model.User;

public interface UserDao {

	public abstract User createUserDao(User user) throws UserException;

	public abstract User readUserByIdDao(int userId) throws UserException;

	public abstract User readUserByNameDao(String userName) throws UserException;

	public abstract User authenticateUserIdAndPasswordDao(int userId, String password) throws UserException;

	public abstract User updateUserDao(User user) throws UserException;

	public abstract int deleteByIdDao(int userId) throws UserException;
}
