package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLConnection {

	public static void main(String[] args) {
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
		String driverName = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/elh8";
		Class.forName(driverName);
		connection = DriverManager.getConnection(url, "root", "root");
		System.out.println(connection != null ? "connection established" : "connection failed");



		} catch (ClassNotFoundException cnfe) {
		System.err.println("There is no respective jars : "
		+ cnfe.getMessage());
		} catch (SQLException se) {// Catching SQL Exception
		System.err.println("SQL Exception :" + se.getMessage());
		} catch (Exception e) {
		System.err.println(e);
		}



		finally {
		try {
		connection.close();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		}
		}
}
