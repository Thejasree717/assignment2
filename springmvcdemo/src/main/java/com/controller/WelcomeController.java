package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller(value="/root")
public class WelcomeController {

	//@GetMapping(value="/hi")
	@RequestMapping(value = "/hi" ,method = RequestMethod.GET)
	public String sayHello() {
		return "success";
	}
	//@GetMapping(value="/bye")
	@RequestMapping(value = "/bye" ,method = RequestMethod.GET)
	public String sayBye() {
		return "thankyou";
	}
}
