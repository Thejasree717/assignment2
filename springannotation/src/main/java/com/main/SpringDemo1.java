package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.model.Employee;

public class SpringDemo1 {

	public static void main(String[] args) {
		
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext("com.model");
		Employee obj = (Employee) applicationContext.getBean("employee");
		obj.setEmpNo(10);
		obj.setEmpName("Injection");
		System.out.println(obj.getEmpNo());
		System.out.println(obj.getEmpName());
		System.out.println("Program Ends");
	}

}
