package com.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

//@Component  //main/controller
//@Service    //com.service
@Repository  //com.dao
public class Employee {

	@Value("14")  
	private int empNo;
	@Value(value="Happy day")
	private String empName;

	/*public Employee() {
		super();
	}*/

	public Employee(@Value("22") int empNo, @Value("Apple") String empName) {
		super();
		System.out.println("Employee constructor : "+empNo+" "+empName);
		this.empNo = empNo;
		this.empName = empName;
	}

	public int getEmpNo() {
		return empNo;
	}

	       //setter injection
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		return "Employee [empNo=" + empNo + ", empName=" + empName + "]";
	}

}
