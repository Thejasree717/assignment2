package com.main;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.model.Department;
import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

public class XMLAndAnnotation {

	
	public static void main(String[] args) {
		
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("hello.xml");
		 Department department = (Department) applicationContext.getBean("department");
		 Scanner scanner = new Scanner(System.in);
		 System.out.println("Enter userId : ");
		 int intvar = scanner.nextInt();
		 System.out.println("Enter password : ");
		 String strVar = scanner.next();
		 UserService userService = new UserServiceImpl();
		 User user = userService.readUser(intvar, strVar);
		 System.out.println("Welcome "+user.getUserId());
		 System.out.println("The end");
		 

	}

}
