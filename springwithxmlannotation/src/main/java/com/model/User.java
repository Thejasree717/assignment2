package com.model;

import org.springframework.stereotype.Component;

@Component
public class User {

	private int userId;
	private String password;

	public User() {
		super();
		System.out.println("User default constructor");
	}

	public User(int userId, String password) {
		super();
		this.userId = userId;
		this.password = password;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
