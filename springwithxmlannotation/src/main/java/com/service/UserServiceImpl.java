package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;          //DI  -->Interface programming
	
	
	@Override
	public User readUser(int userId, String password) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("hello.xml");
		UserDao userDao = (UserDao)applicationContext.getBean("userDaoImpl");
		System.out.println("You are in user service: "+userId+" "+password);
		//UserDao userDao2 = new UserDaoImpl();
		User user = userDao.readDao(userId, password);
		if(user != null) {
			System.out.println("data from dao");
		}else {
			System.out.println("No data");
		}
		return user;
	}

}
