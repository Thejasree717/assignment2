package com.service;

import com.model.User;

public interface UserService {

	public abstract User readUser(int userId, String password);
}
