package com.dao;

import com.model.User;

public interface UserDao {

	public abstract User readDao(int userId, String password);
}
