package com.service;

public class NormalCalculator implements Calculator, MathInterface {

	@Override
	public int add(int num1, int num2) {
		
		return (num1+num2); //test
	}

	@Override
	public int sub(int num1, int num2) {
		
		return MathInterface.super.sub(num1, num2);
	}

}
