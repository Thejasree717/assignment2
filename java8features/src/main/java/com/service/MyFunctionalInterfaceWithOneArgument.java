package com.service;

@FunctionalInterface
public interface MyFunctionalInterfaceWithOneArgument {

	public abstract String sayHello(String str);
}
