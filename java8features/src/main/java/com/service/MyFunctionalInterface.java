package com.service;


//functional interface ==SAM(single abstract method)
@FunctionalInterface
public interface MyFunctionalInterface {

	public abstract void print();
	
	public static final int intVar = 100;

	public default void sayHello() {
		System.out.println("Hello");
	}
	
	public static void sayBye() {
		System.out.println("Thank you");
	}
}
