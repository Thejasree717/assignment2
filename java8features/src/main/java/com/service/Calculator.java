package com.service;

public interface Calculator {

	public static int salary = 5000;
	
	//method declaration and no body { }
	
	public abstract int add(int num1, int num2);  //abstract method
	
	public default int sub(int num1, int num2) {
		return (num1-num2);
	}
	
	public static int mul(int num1, int num2) {
		return num1*num2;
	}
}
