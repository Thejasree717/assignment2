package com.service;

public interface MyFunctionalInterfaceWithTwoArguments {

	public abstract String twoParameterFunction(int a, String b);
}
