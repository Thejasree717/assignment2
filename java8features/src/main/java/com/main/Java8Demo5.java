package com.main;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public class Java8Demo5 {

	public static void main(String[] args) {
		
		BiConsumer<Integer, String> biConsumer = (abc, efg) -> {
			System.out.println(abc +"  " +efg);
		};
		
		biConsumer.accept(2, "Pass a string");
		
		BiFunction<String, Float, String> biFunction = (String abc, Float efg) ->{
			return "answer " + abc +" "+efg;
		};
		
		String val = biFunction.apply("HCL", 12.34f);
		System.out.println("BiFunction: "+val);
		
		Predicate<Integer> pr = a -> (a>18);
		System.out.println(pr.test(20));
	}

}
