package com.main;

import com.service.MyFunctionalInterfaceWithTwoArguments;

public class java8Demo4 {

	public static void main(String[] args) {
		
		MyFunctionalInterfaceWithTwoArguments m2a = (int aaa, String bbb) ->{
			return aaa+" "+bbb;
		};

		System.out.println(m2a.twoParameterFunction(12, "Hello"));
	}

}
