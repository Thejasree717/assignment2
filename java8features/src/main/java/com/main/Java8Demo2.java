package com.main;

import com.service.MyFunctionalInterface;

public class Java8Demo2 {

	public static void main(String[] args) {
		MyFunctionalInterface functionalInterface = () -> {System.out.println("Demo1 lambda");};
		
		functionalInterface.print();
		
		Java8Demo2 java8Demo2 = new Java8Demo2();
		java8Demo2.localFunction();
		java8Demo2.localFunctionTwo(100);
		java8Demo2.localFunctionThree(functionalInterface);

	}
	
	void localFunction() {
		System.out.println("Local function");
	}
	
	void localFunctionTwo(int a) {
		System.out.println("Local function2 :"+a);
	}
	
	void localFunctionThree(MyFunctionalInterface mfi) {
		System.out.println("localFunctionThree output");
		mfi.print();
	}

}
