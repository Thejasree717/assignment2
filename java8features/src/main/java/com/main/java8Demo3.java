package com.main;

import com.service.MyFunctionalInterfaceWithOneArgument;

public class java8Demo3 {

	public static void main(String[] args) {
		
		MyFunctionalInterfaceWithOneArgument oneArg = (String abc) ->{ 
			return "Welcome :"+abc;
		};

		String val = oneArg.sayHello("HCl");
		System.out.println(val);
	}

}
