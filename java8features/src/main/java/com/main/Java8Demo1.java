package com.main;

import com.service.Calculator;
import com.service.MathInterface;
import com.service.NormalCalculator;
import com.service.ScientificCalculator;

public class Java8Demo1 {

	public static void main(String[] args) {
		//static == class level
		Calculator calculator = new NormalCalculator();
		System.out.println(calculator.add(2, 3));
		System.out.println("subtract : "+calculator.sub(10, 3));
		System.out.println(Calculator.mul(3, 4));
		System.out.println(MathInterface.mul(2, 3));
		
		Calculator scCalculator = new ScientificCalculator();
		System.out.println(scCalculator.add(5, 6));
		System.out.println("subtract : "+scCalculator.sub(100, 40));
		
	}

}
