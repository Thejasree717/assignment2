package com.model;

public class Student {
	
	private String studentName;
	private int studentId;
	private float marks;
	public Student() {
		super();
	}
	public Student(String studentName, int studentId, float marks) {
		super();
		this.studentName = studentName;
		this.studentId = studentId;
		this.marks = marks;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public float getMarks() {
		return marks;
	}
	public void setMarks(float marks) {
		this.marks = marks;
	}
	
	
}
