package com.service;

import com.model.Doctor;

public class DoctorService {

	public Doctor searchDoctorByQualification(Doctor[] doctors) {
		Doctor doctor = new Doctor();
		for(int i=0; i< doctors.length; i++) {
			if(doctors[i].getQualification().equals("MBBS")) {
				doctor = doctors[i];
			}
		}
		return doctor;
	}
}
