package com.model;

import java.util.Arrays;

public class Doctor {

	private String doctorName;
	private int doctorId;
	private String Qualification;
	private Patient[] patients;

	public Doctor() {
		super();
	}

	@Override
	public String toString() {
		return "Doctor [doctorName=" + doctorName + ", doctorId=" + doctorId + ", Qualification=" + Qualification
				+ ", patients=" + Arrays.toString(patients) + "]";
	}

	public Doctor(String doctorName, int doctorId, String qualification, Patient[] patients) {
		super();
		this.doctorName = doctorName;
		this.doctorId = doctorId;
		Qualification = qualification;
		this.patients = patients;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public String getQualification() {
		return Qualification;
	}

	public void setQualification(String qualification) {
		Qualification = qualification;
	}

	public Patient[] getPatients() {
		return patients;
	}

	public void setPatients(Patient[] patients) {
		this.patients = patients;
	}

}
