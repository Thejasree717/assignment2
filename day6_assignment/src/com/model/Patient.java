package com.model;

import java.time.LocalDateTime;

public class Patient {

	private String patientName;
	private String sickness;
	private int age;
	private String gender;

	public Patient() {
		super();
	}

	@Override
	public String toString() {
		return "Patient [patientName=" + patientName + ", sickness=" + sickness + ", age=" + age + ", gender=" + gender
				+ "]";
	}

	public Patient(String patientName, String sickness, int age, String gender) {
		super();
		this.patientName = patientName;
		this.sickness = sickness;
		this.age = age;
		this.gender = gender;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getSickness() {
		return sickness;
	}

	public void setSickness(String sickness) {
		this.sickness = sickness;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}