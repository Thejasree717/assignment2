package com.model;

import java.util.Arrays;

public class Department {

	private String deptName;
	private int deptId;
	private Doctor[] doctors;

	public Department() {
		super();
	}

	@Override
	public String toString() {
		return "Department [deptName=" + deptName + ", deptId=" + deptId + ", doctors=" + Arrays.toString(doctors)
				+ "]";
	}

	public Department(String deptName, int deptId, Doctor[] doctors) {
		super();
		this.deptName = deptName;
		this.deptId = deptId;
		this.doctors = doctors;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public Doctor[] getDoctors() {
		return doctors;
	}

	public void setDoctors(Doctor[] doctors) {
		this.doctors = doctors;
	}

}
