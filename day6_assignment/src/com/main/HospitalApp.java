package com.main;

import java.time.LocalDateTime;

import com.model.Department;
import com.model.Doctor;
import com.model.Patient;

public class HospitalApp {

	public static void main(String[] args) {

		System.out.println("Patients");
		Patient patient1 = new Patient("Thejasree", "Fever", 25, "Female");
		System.out.println(patient1);
		Patient patient2 = new Patient("Himaja", "Headache", 23, "Female");
		System.out.println(patient2);
		Patient patient3 = new Patient("Rama", "Covid", 29, "Male");
		System.out.println(patient3);

		Patient[] patients = new Patient[3];
		patients[0] = patient1;
		patients[1] = patient2;
		patients[2] = patient3;

		System.out.println("Many Doctors have many patients");
		Doctor doctor1 = new Doctor("Sita", 100, "MBBS", patients);
		System.out.println(doctor1);
		Doctor doctor2 = new Doctor("Bujji", 101, "Gynocologist", patients);
		System.out.println(doctor2);
		Doctor doctor3 = new Doctor("Balu", 102, "RMP", patients);
		System.out.println(doctor3);

		Doctor[] doctors = new Doctor[3];
		doctors[0] = doctor1;
		doctors[1] = doctor2;
		doctors[2] = doctor3;

		System.out.println("One doctor has many patients");
		System.out.println("Doctor name : " + doctor1.getDoctorName());
		for (int i = 0; i < doctor1.getPatients().length; i++) {
			System.out.println((i + 1) + "." + doctor1.getPatients()[i]);
		}

		System.out.println("Departments");
		Department department1 = new Department("Heart", 001, doctors);
		System.out.println(department1);
		Department department2 = new Department("Skin", 002, doctors);
		System.out.println(department2);
		Department department3 = new Department("Eye", 003, doctors);
		System.out.println(department3);

		System.out.println("One department has many doctors");
		System.out.println("DepartmentId: " + department1.getDeptId());
		System.out.println("Department Name : " + department1.getDeptName());

		for (int i = 0; i < department1.getDoctors().length; i++) {
			System.out.println((i + 1) + "Doctor Name : " + department1.getDoctors()[i].getDoctorName());
			System.out.println("Qualification : " + department1.getDoctors()[i].getQualification());
			System.out.println("DoctorId : " + department1.getDoctors()[i].getDoctorId());

			System.out.println("One doctor has many patients");
			for (int j = 0; j < department1.getDoctors()[i].getPatients().length; j++) {
				System.out.println((j + 1) + ". Patient data");
				System.out.println("Name : " + department1.getDoctors()[i].getPatients()[j].getPatientName());
				System.out.println("Sickness : " + department1.getDoctors()[i].getPatients()[j].getSickness());
				System.out.println("Age : " + department1.getDoctors()[i].getPatients()[j].getAge());
				System.out.println("gender : " + department1.getDoctors()[i].getPatients()[j].getGender());
			}
		}

	}

}
