package com.model;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Student {

	private int studNo;
	private String studName;
	private float marks;

	public Student() {
		super();
		System.out.println("Student Constructor");
	}

	public Student(int studNo, String studName, float marks) {
		super();
		this.studNo = studNo;
		this.studName = studName;
		this.marks = marks;
	}

	public int getStudNo() {
		return studNo;
	}

	public void setStudNo(int studNo) {
		this.studNo = studNo;
	}

	public String getStudName() {
		return studName;
	}

	public void setStudName(String studName) {
		this.studName = studName;
	}

	public float getMarks() {
		return marks;
	}

	public void setMarks(float marks) {
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "Student [studNo=" + studNo + ", studName=" + studName + ", marks=" + marks + "]";
	}
	
	public void hello() {
		System.out.println("replaced InitializingBean()");
	}
	
	public void bye() {
		System.out.println("bye replaced DisposableBean - destroy == close");
	}

	/*@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("InitializingBean()");
		
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("DisposableBean - destroy == close");
		
	}*/

}
