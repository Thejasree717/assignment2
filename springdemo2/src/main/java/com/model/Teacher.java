package com.model;

import java.util.Set;

public class Teacher {

	private int teacherId;
	private String teacherName;
	private String qualification;
	private Set<Student> students; // one to many == has

	public int getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(int teacherId) {
		this.teacherId = teacherId;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Teacher() {
		super();
	}

	public Teacher(int teacherId, String teacherName, String qualification) {
		super();
		this.teacherId = teacherId;
		this.teacherName = teacherName;
		this.qualification = qualification;
	}

	
}
