package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Student;

public class SpringMainDemo1 {

	public static void main(String[] args) {
		
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("springconfig.xml");

		Student student  = (Student) applicationContext.getBean("student");
		
		System.out.println("Roll : "+student.getStudNo());
		System.out.println("Name : "+student.getStudName());
		System.out.println("Marks : "+student.getMarks());
		((ConfigurableApplicationContext)applicationContext).close();
		System.out.println("The End");
	}

}
