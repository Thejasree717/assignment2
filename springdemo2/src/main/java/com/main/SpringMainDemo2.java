package com.main;

import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Student;
import com.model.Teacher;

public class SpringMainDemo2 {

	public static void main(String[] args) {
		
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("springconfig.xml");

		Teacher teacher  = (Teacher) applicationContext.getBean("teacher");
		
		System.out.println("Teacher Id : "+teacher.getTeacherId());
		System.out.println("Teacher Name : "+teacher.getTeacherName());
		System.out.println("Qualification : "+teacher.getQualification());
		
		Set<Student> students = teacher.getStudents();
		for (Student student : students) {
			System.out.println("Student no : "+student.getStudNo());
			System.out.println("Student Name :"+student.getStudName());
			System.out.println("Student mark : "+student.getMarks());
		}
		
		
		((ConfigurableApplicationContext)applicationContext).close();
		System.out.println("The End");
	}

}
