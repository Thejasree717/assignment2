package com.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;

import com.dao.HibernateUtil;
import com.exception.DepartmentException;
import com.exception.EmployeeException;
import com.model.Department;
import com.model.Employee;
import com.service.DepartmentService;
import com.service.DepartmentServiceImpl;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class DepartmentMain {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		EmployeeService employeeService = new EmployeeServiceImpl();
		DepartmentService departmentService = new DepartmentServiceImpl();

		// Creating Employees

		System.out.println("*****Enter the Employee details***********");
		List<Employee> employees = new ArrayList<Employee>();
		for (int i = 0; i < 2; i++) {
			System.out.println("Enter Employee id : ");
			int employeeId = scanner.nextInt();
			System.out.println("Enter Employee name : ");
			String employeeName = scanner.next();
			System.out.println("Enter Employee salary : ");
			double salary = scanner.nextDouble();

			Employee employee = new Employee(employeeId, employeeName, salary);

			try {

				employee = employeeService.saveEmployee(employee);
				if (employee != null) {
					System.out.println("Employee added " + employee);
				}
			} catch (EmployeeException e) {
				System.err.println(e.getMessage());
			}

			employees.add(employee);
		}
		// Creating department
		System.out.println("*****Enter the Department details***********");
		System.out.println("Enter Department id : ");
		int departmentId = scanner.nextInt();
		System.out.println("Enter department name : ");
		String departmentName = scanner.next();

		Department department = new Department(departmentId, departmentName, employees);
		try {
			department = departmentService.saveDepartment(department);
			if (department != null) {
				System.out.println("Department added " + department);
			}
		} catch (DepartmentException e) {
			System.err.println(e.getMessage());
		}

		department.setEmployees(employees);

		// Read data from tables

		System.out.println("**********Read the data from tables*************");
		System.out.println("Enter the departmentId : ");
		int deptId = scanner.nextInt();
		try {
			department = departmentService.readDepartmentById(deptId);
			if (department != null) {
				System.out.println("Department found " + department);
			}
		} catch (DepartmentException e) {
			System.err.println(e.getMessage());
		}

		
		//Update the data
		System.out.println("*******Update the data*************");
		System.out.println("Enter the department id to update : ");
		int deptId2 = scanner.nextInt();
		System.out.println("Enter the department name : ");
		String deptName2 = scanner.next();
		Session session = HibernateUtil.getSession();
		 Department dept = session.get(Department.class, deptId2);
		department = new Department(deptId2, deptName2, dept.getEmployees());
		try {
			department = departmentService.updateDepartment(department);
			if (department != null) {
				System.out.println("Department updated " + department);
			}
		} catch (DepartmentException e) {
			System.err.println(e.getMessage());
		}
		
		//Delete the data
		System.out.println("************Delete the data*************");
		System.out.println("enter the id to delete");
		int deptId3 = scanner.nextInt();
		try {
			Boolean result = departmentService.deleteDepartment(deptId3);
			if (result != false) {
				System.out.println("Department deleted");
			}
		} catch (DepartmentException e) {
			System.err.println(e.getMessage());
		}
	}

}
