package com.service;

import com.dao.DepartmentDao;
import com.dao.DepartmentDaoImpl;
import com.exception.DepartmentException;
import com.model.Department;

public class DepartmentServiceImpl implements DepartmentService {

	@Override
	public Department saveDepartment(Department department) throws DepartmentException {
		DepartmentDao departmentDao = new DepartmentDaoImpl();
		department =departmentDao.saveDepartmentDao(department);
		if(department!=null) {
			return department;
		}else {
			throw new DepartmentException("Department is not added");
		}
	}

	@Override
	public Department readDepartmentById(int departmentId) throws DepartmentException {
		DepartmentDao departmentDao = new DepartmentDaoImpl();
		Department department =departmentDao.readDepartmentByIdDao(departmentId);
		if(department!=null) {
			return department;
		}else {
			throw new DepartmentException("Department not found");
		}
	}

	@Override
	public Department updateDepartment(Department department) throws DepartmentException {
		DepartmentDao departmentDao = new DepartmentDaoImpl();
		department =departmentDao.updateDepartmentDao(department);
		if(department.getDeptId()!=0) {
			return department;
		}else {
			throw new DepartmentException("Department is not updated");
		}
	}

	@Override
	public Boolean deleteDepartment(int departmentId) throws DepartmentException {
		DepartmentDao departmentDao = new DepartmentDaoImpl();
		Boolean result =departmentDao.deleteDepartmentDao(departmentId);
		if(result!=false) {
			return result;
		}else {
			throw new DepartmentException("Department not deleted");
		}
	}

}
