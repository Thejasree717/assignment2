package com.service;

import com.exception.DepartmentException;
import com.model.Department;

public interface DepartmentService {

public abstract Department saveDepartment(Department department)throws DepartmentException;
	
	public abstract Department readDepartmentById(int departmentId) throws DepartmentException;
	
	public abstract Department updateDepartment(Department department) throws DepartmentException;
	
	public abstract Boolean deleteDepartment(int departmentId) throws DepartmentException;
}
