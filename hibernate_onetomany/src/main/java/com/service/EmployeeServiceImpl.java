package com.service;

import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;
import com.exception.EmployeeException;
import com.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee saveEmployee(Employee employee) throws EmployeeException {
		EmployeeDao employeeDao = new EmployeeDaoImpl();
		employee =employeeDao.saveEmployeeDao(employee);
		if(employee!=null) {
			return employee;
		}else {
			throw new EmployeeException("Employee is not added");
		}
	}

	@Override
	public Employee getEmployeebyId(int employeeId) throws EmployeeException {
		EmployeeDao employeeDao = new EmployeeDaoImpl();
		Employee employee =employeeDao.getEmployeebyIdDao(employeeId);
		if(employee!=null) {
			return employee;
		}else {
			throw new EmployeeException("Employee not found");
		}
	}

	@Override
	public Employee updateEmployee(Employee employee) throws EmployeeException {
		EmployeeDao employeeDao = new EmployeeDaoImpl();
		employee =employeeDao.updateEmployeeDao(employee);
		if(employee.getEmpNo()!=0) {
			return employee;
		}else {
			throw new EmployeeException("Employee is not updated");
		}
	}

	@Override
	public Boolean deleteEmployee(int employeeId) throws EmployeeException {
		EmployeeDao employeeDao = new EmployeeDaoImpl();
		Boolean result =employeeDao.deleteEmployeeDao(employeeId);
		if(result!=false) {
			return result;
		}else {
			throw new EmployeeException("Employee not deleted");
		}
	}

}
