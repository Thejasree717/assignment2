package com.service;

import com.exception.EmployeeException;
import com.model.Employee;

public interface EmployeeService {

public abstract Employee saveEmployee(Employee employee) throws EmployeeException;
	
	public abstract Employee getEmployeebyId(int employeeId) throws EmployeeException;
	
	public abstract Employee updateEmployee(Employee employee) throws EmployeeException;
	
	public abstract Boolean deleteEmployee(int employeeId) throws EmployeeException;
}
