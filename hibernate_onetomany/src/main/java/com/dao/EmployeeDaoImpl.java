package com.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.exception.EmployeeException;
import com.model.Employee;

public class EmployeeDaoImpl implements EmployeeDao {

	@Override
	public Employee saveEmployeeDao(Employee employee) throws EmployeeException {
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.save(employee);
			transaction.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
		}
		return employee;
	}

	@Override
	public Employee getEmployeebyIdDao(int employeeId) throws EmployeeException {
		Employee employee = null;
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			employee = session.get(Employee.class, employeeId);
			transaction.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
		}
		return employee;
	}

	@Override
	public Employee updateEmployeeDao(Employee employee) throws EmployeeException {
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.update(employee);
			transaction.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
		}
		return employee;
	}

	@Override
	public Boolean deleteEmployeeDao(int employeeId) throws EmployeeException {
		Session session = HibernateUtil.getSession();
		Employee employee =null;
		Transaction transaction = session.beginTransaction();
		try {
			employee = session.get(Employee.class, employeeId);
			if(employee!=null) {
			session.delete(employee);
			transaction.commit();
			}else {
				return false;
			}
		}catch(HibernateException e) {
			e.printStackTrace();
		}
		return true;
	}

}
