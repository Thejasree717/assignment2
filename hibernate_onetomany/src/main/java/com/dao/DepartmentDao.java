package com.dao;

import com.exception.DepartmentException;
import com.model.Department;

public interface DepartmentDao {

	public abstract Department saveDepartmentDao(Department department)throws DepartmentException;
	
	public abstract Department readDepartmentByIdDao(int departmentId) throws DepartmentException;
	
	public abstract Department updateDepartmentDao(Department department) throws DepartmentException;
	
	public abstract Boolean deleteDepartmentDao(int departmentId) throws DepartmentException;
}
