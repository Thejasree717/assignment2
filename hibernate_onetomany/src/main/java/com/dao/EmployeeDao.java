package com.dao;

import com.exception.EmployeeException;
import com.model.Employee;

public interface EmployeeDao {

	public abstract Employee saveEmployeeDao(Employee employee) throws EmployeeException;
	
	public abstract Employee getEmployeebyIdDao(int employeeId) throws EmployeeException;
	
	public abstract Employee updateEmployeeDao(Employee employee) throws EmployeeException;
	
	public abstract Boolean deleteEmployeeDao(int employeeId) throws EmployeeException;
	
}
