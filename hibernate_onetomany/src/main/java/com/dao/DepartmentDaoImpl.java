package com.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.exception.DepartmentException;
import com.model.Department;
import com.model.Employee;

public class DepartmentDaoImpl implements DepartmentDao {

	@Override
	public Department saveDepartmentDao(Department department) throws DepartmentException {
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.save(department);
			transaction.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return department;
	}

	@Override
	public Department readDepartmentByIdDao(int departmentId) throws DepartmentException {
		Department department = null;
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			department = session.get(Department.class, departmentId);
			transaction.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return department;
	}

	@Override
	public Department updateDepartmentDao(Department department) throws DepartmentException {
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.update(department);
			transaction.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return department;
	}

	@Override
	public Boolean deleteDepartmentDao(int departmentId) throws DepartmentException {
		Session session = HibernateUtil.getSession();
		Department department = null;
		Transaction transaction = session.beginTransaction();
		try {
			department = session.get(Department.class, departmentId);
			if (department != null) {
				List<Employee> deptEmpl = department.getEmployees();
				if (deptEmpl == null) {
					session.delete(department);
					transaction.commit();
				} else {
					return false;
				}
			}else {
				return false;
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return true;
	}

}
