package com.exception;

public class DepartmentException extends Exception {

	private String errorMessage;

	public DepartmentException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	@Override
	public String getMessage() {
		
		return this.errorMessage;
	}
	
}
