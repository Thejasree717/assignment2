package com.exception;

public class EmployeeException extends Exception {
	
	private String errorMessage;

	public EmployeeException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	@Override
	public String getMessage() {
		
		return this.errorMessage;
	}
	
	

}
