package com.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="department",
uniqueConstraints = {@UniqueConstraint(columnNames = "ID")})
public class Department implements Serializable{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="ID", unique = true, nullable=false)
	private int deptId;
	
	@Column(name = "dept_name", length = 25)
	private String deptname;
	
	@OneToMany(cascade = { CascadeType.ALL})
	@JoinTable(name="department_employee", joinColumns = {@JoinColumn(name = "department_id", referencedColumnName="ID")}, inverseJoinColumns = { @JoinColumn(name = "emp_No", referencedColumnName="ID")})
	private List<Employee> employees;

	public Department() {
		super();
	}
	
	
	public Department(int deptId, String deptname) {
		super();
		this.deptId = deptId;
		this.deptname = deptname;
	}



	public Department(int deptId, String deptname, List<Employee> employees) {
		super();
		this.deptId = deptId;
		this.deptname = deptname;
		this.employees = employees;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public String getDeptname() {
		return deptname;
	}

	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	@Override
	public String toString() {
		return "Department [deptId=" + deptId + ", deptname=" + deptname + ", employees=" + employees + "]";
	}

}
