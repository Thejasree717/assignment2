package com.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.exception.ProductException;
import com.model.Product;

public class ProductDaoImpl implements ProductDao {

	@Override
	public Product addProductDao(Product product) throws ProductException {
		Session session = HibernateDemo.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.save(product);
			transaction.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
		}
		return product;
	}

	@Override
	public Product searchProductByIdDao(int productId) throws ProductException {
		Product product = null;
		Session session = HibernateDemo.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			product = session.get(Product.class, productId);
			transaction.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
		}
		return product;
	}

	@Override
	public Product updateProductDao(Product product) throws ProductException {
		Session session = HibernateDemo.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.saveOrUpdate(product);
			transaction.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
		}
		return product;
	}

	@Override
	public Boolean deleteProductDao(int productId) throws ProductException {
		Session session = HibernateDemo.getSession();
		Product product =null;
		Transaction transaction = session.beginTransaction();
		try {
			product = session.get(Product.class, productId);
			if(product!=null) {
			session.delete(product);
			transaction.commit();
			}else {
				return false;
			}
		}catch(HibernateException e) {
			e.printStackTrace();
		}
		return true;
	}

}
