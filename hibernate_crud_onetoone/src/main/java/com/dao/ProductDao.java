package com.dao;

import com.exception.ProductException;
import com.model.Product;

public interface ProductDao {

	public abstract Product addProductDao(Product product) throws ProductException;

	public abstract Product searchProductByIdDao(int productid) throws ProductException;

	public abstract Product updateProductDao(Product product) throws ProductException;

	public abstract Boolean deleteProductDao(int productId) throws ProductException;

}
