package com.service;

import com.exception.ProductException;
import com.model.Product;

public interface ProductService {

	public abstract Product addProduct(Product product) throws ProductException;

	public abstract Product searchProductById(int productid) throws ProductException;

	public abstract Product updateProduct(Product product) throws ProductException;

	public abstract Boolean deleteProduct(int productId) throws ProductException;
}
