package com.service;

import com.dao.ProductDao;
import com.dao.ProductDaoImpl;
import com.exception.ProductException;
import com.model.Product;

public class ProductServiceImpl implements ProductService {

	@Override
	public Product addProduct(Product product) throws ProductException {
		ProductDao productDao = new ProductDaoImpl();
		product =productDao.addProductDao(product);
		if(product!=null) {
			return product;
		}else {
			throw new ProductException("Product is not added");
		}
		
	}

	@Override
	public Product searchProductById(int productid) throws ProductException {
		ProductDao productDao = new ProductDaoImpl();
		Product product =productDao.searchProductByIdDao(productid);
		if(product!=null) {
			return product;
		}else {
			throw new ProductException("Product not found");
		}
	}

	@Override
	public Product updateProduct(Product product) throws ProductException {
		ProductDao productDao = new ProductDaoImpl();
		product =productDao.updateProductDao(product);
		if(product.getProductId()!=0) {
			return product;
		}else {
			throw new ProductException("Product is not updated");
		}
	}

	@Override
	public Boolean deleteProduct(int productId) throws ProductException {
		ProductDao productDao = new ProductDaoImpl();
		Boolean result =productDao.deleteProductDao(productId);
		if(result!=false) {
			return result;
		}else {
			throw new ProductException("Product not deleted");
		}
		
	}

}
