package com.main;

import java.util.Scanner;

import com.exception.ProductException;
import com.model.Product;
import com.service.ProductService;
import com.service.ProductServiceImpl;

public class ProductApp {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		ProductService productService = new ProductServiceImpl();

		// Create product

		System.out.println("*****Enter the Product details***********");
		System.out.println("Enter product id : ");
		int productId = scanner.nextInt();
		System.out.println("Enter product name : ");
		String productName = scanner.next();
		System.out.println("Enter product price : ");
		double price = scanner.nextDouble();
		Product product = new Product(productId, productName, price);
		try {
			product = productService.addProduct(product);
			if (product != null) {
				System.out.println("Product added " + product);
			}
		} catch (ProductException e) {
			System.err.println(e.getMessage());
		}

		// Search for product using id

		System.out.println("*********Search for a product using id********");
		System.out.println("Enter the product id : ");
		int productId1 = scanner.nextInt();
		try {
			product = productService.searchProductById(productId1);
			if (product != null) {
				System.out.println("Product found " + product);
			}
		} catch (ProductException e) {
			System.err.println(e.getMessage());
		}

		// Update the product

		System.out.println("********Update the product**********");
		System.out.println("Enter the productId to update : ");
		int productId2 = scanner.nextInt();
		System.out.println("Enter the product name : ");
		String productname2 = scanner.next();
		System.out.println("Enter the price : ");
		double price2 = scanner.nextDouble();
		product = new Product(productId2, productname2, price2);
		try {
			product = productService.updateProduct(product);
			if (product != null) {
				System.out.println("Product updated " + product);
			}
		} catch (ProductException e) {
			System.err.println(e.getMessage());
		}

		// Delete the product

		System.out.println("*******Delete the product***********");
		System.out.println("Enter the productId to delete : ");
		int productid3 = scanner.nextInt();
		try {
			Boolean result = productService.deleteProduct(productid3);
			if (result != false) {
				System.out.println("Product deleted");
			}
		} catch (ProductException e) {
			System.err.println(e.getMessage());
		}
	}

}
