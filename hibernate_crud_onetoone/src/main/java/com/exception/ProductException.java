package com.exception;

public class ProductException extends Exception {

	private String errorMessage;

	public ProductException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	@Override
	public String getMessage() {

		return errorMessage;
	}

}
