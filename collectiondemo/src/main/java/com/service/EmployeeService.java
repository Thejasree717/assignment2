package com.service;

import java.util.List;

import com.model.Employee;

public interface EmployeeService {
	
	public abstract Employee searchEmployeeById(List listOfEmployee, int id);
	
	public abstract List searchEmployeeByName(List listOfEmployee, String name);

}
