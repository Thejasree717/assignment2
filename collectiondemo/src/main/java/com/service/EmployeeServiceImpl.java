package com.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee searchEmployeeById(List listOfEmployee, int id) {
		
		Employee employee = null;
		for (Iterator iterator = listOfEmployee.iterator(); iterator.hasNext();) {
			Employee object = (Employee) iterator.next();
			if(object.getEmpNo() == id) {
				employee = object;
			}
			
		}
		return employee;
	}

	@Override
	public List searchEmployeeByName(List listOfEmployee, String name) {
		
		List employees = new ArrayList();
		for (Iterator iterator = listOfEmployee.iterator(); iterator.hasNext();) {
			Employee object = (Employee) iterator.next();
			if(object.getEmpName().equals(name)) {
				
				employees.add(object);
			}
			
		}
		return employees;
	}

}
