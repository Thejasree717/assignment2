package com.main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class HrApp {

	public static void main(String[] args) {
		
		Employee employee1 = new Employee(10, "Ten", 1000f);
		Employee employee2 = new Employee(20, "Twenty", 2000f);
		Employee employee3 = new Employee(30, "Thirty", 3000f);
		Employee employee4 = new Employee(40, "Fourty", 4000f);
		
		/*Set set = new HashSet<>();
		set.add(employee1);
		set.add(employee2);
		set.add(employee3);
		set.add(employee4);
		set.add(employee1); */
		
		List list = new ArrayList();
		list.add(employee1);
		list.add(employee2);
		list.add(employee3);
		list.add(employee4);
		list.add(employee4);
		
		EmployeeService employeeService = new EmployeeServiceImpl();
		Employee ans = employeeService.searchEmployeeById(list, 20);
		System.out.println(ans.getEmpNo());
		System.out.println(ans.getEmpName());
		
		List ans2 = employeeService.searchEmployeeByName(list, "Ten");
		for (Iterator iterator = ans2.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			if (object instanceof Employee) {
				Employee employees = (Employee) object;
				System.out.println("Employee number : " +employees.getEmpNo());
				System.out.println("Employee name : " +employees.getEmpName());
			}
			
		}
		
		System.out.println("how many object??" +list.size());
		System.out.println("Display all employees");
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			 if (object instanceof Employee) {
				Employee employee = (Employee) object;
				System.out.println("Employee number : " +employee.getEmpNo());
				System.out.println("Employee name : " +employee.getEmpName());
			}
			
		}

	}

}
