package com.main;

import java.util.HashMap;
import java.util.Map;

import com.model.Employee;

public class MapDemo {

	public static void main(String[] args) {
		
		Map map = new HashMap();
		
		Employee employee1 = new Employee(10, "Ten", 1000f);
		Employee employee2 = new Employee(20, "Twenty", 2000f);
		Employee employee3 = new Employee(30, "Thirty", 3000f);
		Employee employee4 = new Employee(40, "Fourty", 4000f);
		
		/* map.put("one", employee1);
		map.put("five", employee2);
		map.put("ten", employee3);
		map.put("fifteen", employee4); */
		
		map.put(employee1.getEmpNo(), employee1);
		map.put(employee2.getEmpNo(), employee2);
		map.put(employee3.getEmpNo(), employee3);
		map.put(employee4.getEmpNo(), employee4);
		
		Employee data = (Employee) map.get(10);
		System.out.println(data.getEmpNo());
		System.out.println(data.getEmpName());

	}

}
