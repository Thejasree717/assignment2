package com.main;

import java.util.Scanner;

import com.exception.UserException;
import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

public class LoginApp {

	public static void main(String[] args) {

		Scanner input = null;
		UserService userService = null;
		try {
			input = new Scanner(System.in);
			System.out.println("Enter userid: ");
			int userId = input.nextInt();
			System.out.println("Enter Password: ");
			String password = input.next();
			userService = new UserServiceImpl();
			try {
				User user = userService.authorise(userId, password);
				// System.out.println("User id : "+user.getUserId());
				if (user != null) {
					System.out.println("Welcome : " + user.getUserName());
				}
			} catch (UserException e) {
				System.err.println(e.getMessage());

			}
		} finally {
			input = null;
			userService = null;
		}
	}

}
