package com.service;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.exception.UserException;
import com.model.User;

public class UserServiceImpl implements UserService {

	@Override
	public User authorise(int userId, String password) throws UserException {
		
		User user = null;
		int lengthOfUserId = String.valueOf(userId).length();
		int lengthOfPassword = password.length();
		if (lengthOfUserId >= 5 && lengthOfPassword >= 5) {
			UserDao userDao = new UserDaoImpl();
			user = userDao.authoriseDao(userId, password);
		} else {

			throw new UserException("Invalid userid or password");
		}
		return user;
	}

}
