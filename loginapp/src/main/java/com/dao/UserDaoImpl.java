package com.dao;

import java.util.ArrayList;
import java.util.List;

import com.exception.UserException;
import com.model.User;


public class UserDaoImpl implements UserDao {
	
	public List<User> init()
	{
		User user1 = new User(11111,"First", "First1234");
		User user2 = new User(22222, "Second", "Second1234");
		User user3 = new User(33333, "Third", "Third1234");
		User user4 = new User(44444, "Fourth", "Fourth1234");
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		users.add(user3);
		users.add(user4);
		
		return users;
	}

	@Override
	public User authoriseDao(int userId, String password) throws UserException {
		System.out.println("Data in Dao : "+userId +" "+password);
		User user = null;
		List<User> users = this.init();
		for (User user2 : users) {
			System.out.println(user2);
			if(user2.getUserId() == userId && user2.getPassword().equals(password)) {
				user = user2;
			}
			
		}
		if(user!= null) {
			return user;
		}
		else {
			throw new UserException("No such Data");
		}
	}

}
