package com.dao;

import com.exception.UserException;
import com.model.User;

public interface UserDao {
	
	public abstract User authoriseDao(int userId, String password) throws UserException;

}
