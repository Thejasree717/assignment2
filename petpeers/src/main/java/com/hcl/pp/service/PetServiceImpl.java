package com.hcl.pp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.Exception.PetException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.validator.PetValidator;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepository petRepository;

	@Autowired
	private PetValidator petValidator;

	@Override
	public Pet savePet(Pet pet) throws PetException{
		Pet pet1 = null;
		boolean is = false;
		is = petValidator.validate(pet);
		if (is == true) {
			pet1 = petRepository.save(pet);
		}if(pet1 == null) {
			throw new PetException("Pet is not added");
		}
		return pet1;
	}

	@Override
	public List<Pet> getAllPets() throws PetException {
		List<Pet> pets = petRepository.findAll();
		if(pets == null) {
			throw new PetException("Pets not found");
		}
		return pets;
	}

	@Override
	public Pet getpetById(long id) throws PetException {
		Pet pet = null;
		Optional<Pet> optional = petRepository.findById(id);
		if (optional.isPresent()) {
			pet = optional.get();
		}if(pet == null) {
			throw new PetException("No pet found with id "+id);
		}
		return pet;
	}

	@Override
	public Pet updatePet(Pet pet) throws PetException {
		Pet finalPet = null;
		Optional<Pet> optional = petRepository.findById(pet.getId());
		if (optional.isPresent()) {
			Pet dbObj = optional.get();
			dbObj.setName(pet.getName());
			dbObj.setAge(pet.getAge());
			dbObj.setPlace(pet.getPlace());
			dbObj.setOwner(pet.getOwner());
			finalPet = petRepository.save(pet);
		}if(finalPet == null) {
			throw new PetException("Pet is not updated");
		}
		return finalPet;
	}

	@Override
	public int deletePetById(long id) throws PetException {
		if( petRepository.findById(id) != null) {
		  petRepository.deleteById(id);
		}else {
			throw new PetException("Pet not deleted");
		}
		return 0;
	}

}
