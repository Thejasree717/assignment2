package com.hcl.pp.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.Exception.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.repository.UserRepository;
import com.hcl.pp.validator.UserValidator;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PetRepository petRepository;

	@Autowired
	private UserValidator userValidator;

	@Override
	public User addUser(User user) throws UserException {
		User user1 = null;
		boolean is = false;
		is = userValidator.validateUser(user);
		if (is == true) {
			user1 = userRepository.save(user);
		}
		if (user1 == null) {
			throw new UserException("User is not added");
		}
		return user1;
	}

	@Override
	public List<User> listUsers() throws UserException {
		List<User> users = userRepository.findAll();
		if (users == null) {
			throw new UserException("Users not found");
		}
		return users;
	}

	@Override
	public Set<Pet> getMyPets(long userId) throws UserException {
		User user = userRepository.findById(userId).get();
		if (user == null) {
			throw new UserException("User data doesnot exists with id: " + userId);
		}
		return user.getPets();
	}

	@Override
	public Pet buyPet(long userId, long petId) throws UserException {
		User user = userRepository.findById(userId).get();
		if (user == null) {
			throw new UserException("User data doesn't exists with id: " + userId);
		}
		Pet pet = petRepository.findById(petId).get();
		if (pet == null) {
			throw new UserException("Pet data doesn't exists with id : " + petId);
		}
		pet.setOwner(user);
		return petRepository.save(pet);
	}

	@Override
	public int removeUser(long userId) throws UserException {
		User user = null;
		Optional<User> optional = userRepository.findById(userId);
		if (optional.isPresent()) {
			user = optional.get();

			if (user.getPets().size() == 0) {
				userRepository.deleteById(userId);
			} else {
				throw new UserException("User is not deleted");
			}
		}
		return 0;
	}

	@Override
	public User login(String userName, String userPassword) throws UserException {
		User user = userRepository.findByUserName(userName, userPassword);
		if (user.getUserName().equals(userName) && user.getUserPassword().equals(userPassword)) {
			return user;
		} else {
			throw new UserException("No user found with the given username and password");
		}
	}

	@Override
	public boolean validation(String userName, String userPassword) throws UserException {
		boolean result = false;
		int lengthOfUserName = userName.length();
		int lengthOfPassword = userPassword.length();
		if (lengthOfUserName >= 5 && lengthOfPassword >= 6) {
			result = true;
		}
		if (result != false) {
			return result;
		} else {
			throw new UserException("Validation failed please give valid userName and password");
		}

	}

}
