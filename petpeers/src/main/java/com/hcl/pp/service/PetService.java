package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.Exception.PetException;
import com.hcl.pp.model.Pet;

public interface PetService {

	public abstract Pet savePet(Pet pet) throws PetException;

	public abstract List<Pet> getAllPets() throws PetException;

	public abstract Pet getpetById(long id) throws PetException;

	public abstract Pet updatePet(Pet pet) throws PetException;

	public abstract int deletePetById(long id) throws PetException;
}
