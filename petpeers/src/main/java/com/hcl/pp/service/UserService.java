package com.hcl.pp.service;

import java.util.List;
import java.util.Set;

import com.hcl.pp.Exception.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;

public interface UserService {

	public abstract User addUser(User user) throws UserException;

	public abstract List<User> listUsers() throws UserException;

	public abstract Set<Pet> getMyPets(long userId) throws UserException;

	public abstract Pet buyPet(long userId, long petId) throws UserException;

	public abstract int removeUser(long userId) throws UserException;

	public User login(String userName, String userPassword) throws UserException;

	public boolean validation(String userName, String userPassword) throws UserException;

}
