package com.hcl.pp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.pp.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query(value = "select *from pet_user where user_name = :userName AND user_password = :userPassword", nativeQuery = true)
	public abstract User findByUserName(@Param("userName") String userName, @Param("userPassword") String userPassword);
}
