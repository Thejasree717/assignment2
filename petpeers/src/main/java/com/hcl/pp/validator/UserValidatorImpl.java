package com.hcl.pp.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hcl.pp.model.User;
import com.hcl.pp.repository.UserRepository;

@Component
public class UserValidatorImpl implements UserValidator {

	private static final Logger LOGGER = LogManager.getLogger(UserValidatorImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Override
	public boolean validateUser(User user) {
		boolean result = false;
		User res = userRepository.findByUserName(user.getUserName(), user.getUserPassword());
		if (res == null) {
			if (user.getUserPassword().equals(user.getConfirmPassword())) {
				LOGGER.info("user created");
				return result = true;
			} else {
				LOGGER.info("Passwords do not match");
				return false;
			}
		} else {
			if (res.getUserName().equals(user.getUserName()) && res.getUserPassword().equals(user.getUserPassword())) {
				LOGGER.info("User name already exists");
				return false;
			}

		}
		return result;

	}

}
