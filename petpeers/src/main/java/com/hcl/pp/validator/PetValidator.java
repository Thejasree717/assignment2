package com.hcl.pp.validator;

import com.hcl.pp.model.Pet;

public interface PetValidator {

	public abstract boolean validate(Pet pet);
}
