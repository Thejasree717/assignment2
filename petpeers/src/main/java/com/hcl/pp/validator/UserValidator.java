package com.hcl.pp.validator;

import com.hcl.pp.model.User;

public interface UserValidator {

	public boolean validateUser(User user);
}
