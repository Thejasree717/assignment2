package com.hcl.pp.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.sun.istack.internal.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "PETS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pet implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(length = 5, name = "ID")
	private long id;

	@NotEmpty(message = "Please enter pet name")
	@Size(min = 2)
	@Column(name = "PET_NAME", length = 55, nullable = false)
	private String name;

	@NotNull
	@Column(length = 2, name = "PET_AGE")
	private int age;

	@NotEmpty(message = "Please enter pet place")
	@Column(length = 55, name = "PET_PLACE")
	private String place;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "owner_id")
	private User owner;

}
