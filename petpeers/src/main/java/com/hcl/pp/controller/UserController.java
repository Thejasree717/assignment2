package com.hcl.pp.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.pp.Exception.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.service.UserService;
import com.hcl.pp.service.UserServiceImpl;
import com.hcl.pp.validator.UserValidator;

@RestController
@RequestMapping("/user")
public class UserController {

	private static final Logger LOGGER = LogManager.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@PostMapping(value = "/add")
	public User addUser(@Valid @RequestBody User user) throws UserException {
		return userService.addUser(user);

	}

	@GetMapping(value = "/users")
	public List<User> listUsers() throws UserException {
		return userService.listUsers();
	}

	@GetMapping(value = "/pets/myPets/{userId}")
	public Set<Pet> myPets(@PathVariable("userId") long userId) throws UserException {
		return userService.getMyPets(userId);
	}

	@GetMapping(value = "/pets/buyPet/{userId}/{petId}")
	public Pet buyPet(@PathVariable("userId") long userId, @PathVariable("petId") long id) throws UserException {
		return userService.buyPet(userId, id);
	}

	@DeleteMapping(value = "/delete/{userId}")
	public int deleteUser(@PathVariable("userId") long userId) throws UserException {
		int val = userService.removeUser(userId);
		return val;
	}

	@GetMapping(value = "/login/{userName}/{userPassword}")
	public User login(@PathVariable String userName, @PathVariable String userPassword) throws UserException {
		User user = null;
		if (userName != null && userPassword != null) {
			boolean validation = userService.validation(userName, userPassword);
			if (validation == true) {
				user = userService.login(userName, userPassword);
				if (user != null) {
					LOGGER.info("Hi " + user.getUserName() + " Welcome to HCL");
					return user;
				}
			}
		}
		{
			throw new UserException("Enter valid username and password");
		}
	}
}
