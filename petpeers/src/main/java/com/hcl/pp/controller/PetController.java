package com.hcl.pp.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.Exception.PetException;
import com.hcl.pp.Exception.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.service.PetService;
import com.hcl.pp.service.UserService;

@RestController
@RequestMapping("/pets")
public class PetController {

	@Autowired
	private PetService petService;

	@Autowired
	private UserService userservice;

	@PostMapping(value = "/addPet")
	public ResponseEntity<Pet> addPet(@Valid @RequestBody Pet pet) throws PetException {
		pet = petService.savePet(pet);
		ResponseEntity<Pet> responseEntity = new ResponseEntity<>(pet, HttpStatus.CREATED);
		return responseEntity;
	}

	@GetMapping(value = "/pets")
	public List<Pet> petHome() throws PetException {
		return petService.getAllPets();
	}

	@GetMapping(value = "/petDetail/{petId}")
	public Pet petDetail(@PathVariable("petId") long id) throws PetException {
		return petService.getpetById(id);
	}

	@GetMapping(value = "/myPets/{userId}")
	public Set<Pet> myPets(@PathVariable("userId") long userId) throws PetException, UserException {
		return userservice.getMyPets(userId);
	}

	@PutMapping(value = "updatePet")
	public Pet updatepetDetails(@RequestBody Pet pet) throws PetException {
		return petService.updatePet(pet);
	}

	@DeleteMapping(value = "deletePet/{petId}")
	public String deletePetById(@PathVariable("petId") long id) throws PetException {
		String message = null;
		int val = petService.deletePetById(id);
		if (val == 1) {
			message = "Pet deleted";
		}
		return message;
	}
}
