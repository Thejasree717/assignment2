package com.hcl.pp.Exception;

public class PetException extends Exception {

	private String errorMessage;

	public PetException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	@Override
	public String getMessage() {

		return this.errorMessage;
	}
}
