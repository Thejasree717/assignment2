package com.springbootdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springbootdemo.model.Employee;
import com.springbootdemo.service.EmployeeService;

@RestController
@RequestMapping(value="hcl")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping(value="emp")
	public Employee getEmployee() {
		return new Employee(10, "Ten", 1010.10f);
	}
	
	@PostMapping(value="create")
	public ResponseEntity<Employee> create(@RequestBody Employee employee){
		Employee employee2 =  employeeService.createEmployee(employee);
		return new ResponseEntity<Employee>(employee2, HttpStatus.CREATED);
	}
	
	@GetMapping(value="read1/{data}")
	public ResponseEntity<Employee> readOne(@PathVariable("data") int empId){
	Employee employee =	employeeService.readById(empId);
	return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}
	
	@GetMapping(value="read2/{data}")
	public ResponseEntity<Employee> readTwo(@PathVariable("data") String empName){
		Employee employee =	employeeService.readByName(empName);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
		}
	
	@PutMapping(value="update")
	public ResponseEntity<Employee> update(@RequestBody Employee employee){
		Employee employee2 = employeeService.updateEmployee(employee);
		return new ResponseEntity<Employee>(employee2, HttpStatus.OK);
		}
	
	@DeleteMapping(value="delete/{empName}")
	public ResponseEntity<Integer> delete(@PathVariable String empName){
		int var = employeeService.deleteByName(empName);
		return new ResponseEntity<Integer>(var, HttpStatus.GONE);
	}
}
