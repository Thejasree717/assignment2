package com.springbootdemo.service;

import java.util.List;

import com.springbootdemo.model.Employee;

public interface EmployeeService {

	public abstract Employee createEmployee(Employee employee);

	public abstract List<Employee> readAllEmployee();

	public abstract Employee readById(int empId);
	
	public abstract Employee readByName(String empName);

	public abstract Employee updateEmployee(Employee employee);

	public abstract int deleteById(int empId);

	public abstract int deleteByName(String empName);
}
