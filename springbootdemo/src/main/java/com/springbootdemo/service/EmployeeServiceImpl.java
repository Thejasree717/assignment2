package com.springbootdemo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.springbootdemo.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee createEmployee(Employee employee) {
		System.out.println("Create in employee service "+employee.getEmpNo());
		System.out.println("Create in employee service "+employee.getEmpName());
		System.out.println("Create in employee service "+employee.getSalary());
		return employee;
	}

	@Override
	public List<Employee> readAllEmployee() {
		Employee employee1 = new Employee(1, "one", 1111f);
		Employee employee2 = new Employee(2, "two", 2222f);
		Employee employee3 = new Employee(3, "three", 3333f);
		
		List<Employee> employees = new ArrayList<Employee>();
		
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		
		return employees;
	}

	@Override
	public Employee readById(int empId) {
		Employee employee =null;
		if(empId == 10) {
			employee = new Employee(10, "ten", 1010.10f);
			
		}if(empId == 20) {
			employee = new Employee(20, "twenty", 2020.10f);
			
		}
		return employee;
	}

	@Override
	public Employee readByName(String empName) {
		Employee employee =null;
		if(empName.equals("hello")) {
			employee = new Employee(10, "hello", 1010f);
		}if(empName.equals("world")) {
			employee = new Employee(20, "world", 2020f);
		}
		return employee;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		System.out.println("Update in service layer "+ employee.getEmpNo());
		return employee;
	}

	@Override
	public int deleteById(int empId) {
		System.out.println("Delete in service layer " + empId); //logic
		return 1;
	}

	@Override
	public int deleteByName(String empName) {
		System.out.println("Delete with name in service layer " + empName); //logic
		return 2;
	}

}
