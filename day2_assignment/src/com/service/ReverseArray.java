package com.service;

public class ReverseArray {

	public static void main(String[] args) {
		
		ArrayData arrayData = new ArrayData();
		System.out.println("Reversed array is : ");
		System.out.println(arrayData.var.length);
		
		for (int i = arrayData.var.length-1; i>=0; i--) {
			
			System.out.println(arrayData.var[i]);
		}
	}

}
