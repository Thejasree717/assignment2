package com.service;

public class HigherSalary {

	public static void main(String[] args) {
		Employee employee1 = new Employee();
		employee1.age = 25;
		employee1.name = "XYZ";
		employee1.designation = "Software engineer";
		employee1.phoneNo = "9876504321";
		employee1.salary = 2000f;
		
		Employee employee2 = new Employee();
		employee2.age = 28;
		employee2.name = "ABC";
		employee2.designation = "Software developer";
		employee2.phoneNo = "9874506321";
		employee2.salary = 4000f;
		
		if(employee1.salary > employee2.salary) {
			System.out.println(employee1.name +" is getting higher salary");
		}
		
		if(employee2.salary > employee1.salary) {
			System.out.println(employee2.name +" is getting higher salary");
		}
	}

}
